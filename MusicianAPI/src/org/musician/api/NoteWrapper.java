package org.musician.api;

/**
 *
 * @author Geertjan
 */
public class NoteWrapper {
    
    Note note;
    int pitch;

    public NoteWrapper(Note note, int pitch) {
        this.note = note;
        this.pitch = pitch;
    }

    public Note getNote() {
        return note;
    }

    public int getPitch() {
        return pitch;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

}
