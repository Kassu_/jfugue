package org.musician.api;

import java.io.Serializable;

/**
 *
 * @author Pierre Matthijs
 */
public class InstrumentCategory implements Serializable {
    
    private String instrumentCategory;
    
    /** Creates a new instance of InstrumentCategory */
    public InstrumentCategory() {
    }

    public String getInstrumentCategory() {
        return instrumentCategory;
    }

    public void setInstrumentCategory(String instrumentCategory) {
        this.instrumentCategory = instrumentCategory;
    }
    
}
